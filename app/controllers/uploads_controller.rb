class UploadsController < ApplicationController
  def index
    @uploads = Upload.all
  end

  def new
    @upload = Upload.new
  end

  def create
    @upload = Upload.new(title: params[:title], file: params[:file])
    @upload.save
  end

  def destroy
  end

  def show
  end
end
