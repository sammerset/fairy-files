class CreateUploads < ActiveRecord::Migration
  def change
    create_table :uploads do |t|
      t.text :title
      t.string :file

      t.timestamps
    end
  end
end
